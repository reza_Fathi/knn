
\section{introduction}
\label{sec:intro}

The $K$-nearest neighbors is a well-studied problem
in machine learning with numerous applications. (e.g., \cite{shalev2014understanding}). It
is a non-parametric method used for classification and regression, especially in application such as pattern recognition.
The algorithmic problem is as follows.
We are given a (training) set of $n$ data points ($n$
can be potentially very large and/or each point can be
in a high dimensional space) with labels and a query point $q$. The goal is to assign a label to $q$ based on the labels of the
{\em $K$-nearest points to the query}. \longOnly{Typically, the $n$ points may be in some $d$-dimensional space and we assume that there
is a metric that given two points computes the distance
between the two points (commonly used metrics include Euclidean distance or Hamming distance). }In the {\em classification} problem, one can use the majority
of the labels of the $K$-nearest neighbors to assign
a label to $q$. In the {\em regression} problem, one can assign
the average of the labels (assuming that these are values)
to $q$.

In this paper \onlyShort{(see full version \cite{disknn2020})}, we study distributed algorithms for the $K$-nearest neighbors problem motivated by Big Data and privacy applications. When the data size is very large \longShort{(even storing all points in a single machine might be memory intensive), then distributed computation using multiple machines is helpful. Another even more relevant motivation for distributed computing is that in many instances data is naturally distributed at $k$-sites (e.g., patients data in different hospitals) and it is too costly or undesirable (say for privacy reasons) to transfer all the data to a single location for computing the answer.}{or naturally distributed at $k$-sites (e.g., patients data in different hospitals), then distributed computation using multiple machines is helpful.} 

\longShort{\subsection{Model}}{\noindent{\textbf{Model:}}}
We study the $K$-nearest neighbors problem
in the {\em $k$-machine model}, a model for distributed large-scale data.  (Henceforth, to avoid confusion, between
$K$ and $k$, which are unrelated we
will say $\ell$-nearest neighbors).
The $k$-machine model was introduced in~\cite{klauck2015distributed} and further investigated
in~\cite{chung2015distributed, PanduranganRS16Journal,bandyapadhyay2018near,pandurangan2018distributed}.
The model consists of a set of $k \geq 2$ machines $\{M_1,M_2,\dots,M_k\}$ 
that are pairwise interconnected by bidirectional point-to-point communication links.
Each machine executes an instance of a distributed algorithm. The computation advances
in synchronous rounds where, in each round, machines can exchange messages over their
communication links and perform some local computation. Each link is assumed to have
a bandwidth of $B$ bits per round, i.e., $B$ bits can be
transmitted over each link in each round;  unless otherwise stated, we assume $B = \Theta(\log n)$. Machines do not share any memory and have no other
means of communication. We assume that each machine has access to a private source of true random bits.

Local computation within a machine is considered to happen instantaneously at zero cost, while the exchange of messages between machines is the costly operation.
However, we note that in all the algorithms of this paper, every machine in every round performs lightweight computations. \onlyLong{In particular, these computations are bounded by (essentially) linear in the size of the input assigned to that machine.}
The goal is to design algorithms that take {\em as few communication rounds as possible}.

\longOnly{
We say that algorithm $\cA$ has \emph{$\epsilon$-error} if, in any run of $\cA$, the output of the machines corresponds to a correct solution with probability at least $1 - \epsilon$.
To quantify the performance of a randomized (Monte Carlo) algorithm $\cA$, we define the \emph{round complexity of $\cA$}  to be the worst-case number of rounds required by any machine when executing $\cA$. 

For the $\ell-$nearest neighbors problem in the $k$-machine model, We assume that the $n$ points are distributed (in a balanced fashion) among the $k$ 
machines, i.e., each machine has $O(n/k)$ points
(adversarially distributed) and the goal is to compute an answer
given a query point in as few rounds as possible.
We assume that the query point is given to all  machines (or equivalently to a single machine, which can broadcast
to all machines in a round).
}
\longShort{\subsection{The Selection Problem}}{\noindent{\textbf{The Selection Problem:}}}
We note that the $\ell$-nearest neighbors problem really boils down to the {\em selection} problem, where
the goal is to find the $\ell$-smallest value in a 
set of $n$ values. The selection problem has a (somewhat non-trivial) linear time deterministic  algorithm \cite{Cormenbook} as well as simple randomized 
algorithm in the sequential setting. For the
$\ell$-nearest neighbors, one can reduce it to
the selection problem by computing the distance of the query point 
to all the points and then finding the $\ell$-smallest distance among these $n$ distance values. 
All these can be done in $O(n)$ time sequentially.

\longShort{\subsection{Our Results}}{\noindent{\textbf{Our Results:}}}
In this paper, we present efficient bounds
for the $\ell$-nearest neighbors or equivalently
to the $\ell$-selection problem.
Our main result is a randomized algorithm in the $k$-machine model that runs in $O(\log \ell)$ communication rounds with high probability (regardless of the number of
machines $k$). The message complexity of the algorithm
is also small taking only $O(k\log \ell)$ messages. Note that if $\ell$ is not very large (which is generally true in practice), then these bounds imply very fast algorithms requiring only a small number of rounds regardless of the number of points and the number of sites (machines). 

Our bounds are essentially the best possible for comparison-based\footnote{We conjecture that the lower bound holds even for non-comparison based algorithms.} algorithms, i.e., algorithms that use only comparison operations ($\leq, \geq, =$) between elements to distinguish the ordering among them. This is due to the existence of a lower bound of $\Omega(\log n)$ communication rounds (if only one element is allowed to be exchanged per round) for finding the {\em median} of $2n$ elements distributed evenly among two processors \cite{rodeh}. 

We also implement and test our algorithm in a distributed cluster, and show that it performs well compared to a simple algorithm that sends
$\ell$ nearest points from each machine to a single machine
which then computes the answer.
\onlyLong{
In the  simple algorithm each machine locally finds its $\ell$-nearest  points to the query, gathers them on a single machine, and then finds the final $\ell$-nearest points among these $k\ell$ points. Note that this takes
$O(\ell)$ rounds in the $k$-machine model --- exponentially more than our algorithm.}


% We actually present two algorithms for ease of exposition. Our first algorithm runs in $O(\log k + \log \ell)$ rounds and solves the $\ell$-selection problem. This algorithm is the distributed variant of the randomized sequential selection algorithm (see e.g., \cite{Cormenbook}). We then present an improved algorithm that runs in $O(\log \ell)$ rounds which is independent of the number of machines running the algorithm. This algorithm uses a simple sampling technique to further eliminate points. In applications where $\ell$ is much smaller than $k$, the number of machines, the second algorithm will be helpful.


%Finally we present an experimental evaluation of our algorithm by implementing and testing it in a distributed cluster. We show that our algorithm has a significant speed up  compared to the simple solution described earlier. 
\onlyLong{
\longShort{\subsection{Related Work}}{\noindent{\textbf{Related Work:}}}
 Methods in \cite{cahsai2017scaling, yang2018efficient} use binary search over the distance of the points from the query point.
 The work of \cite{saukas1998efficient} which is the closest to the spirit of our work, proposed a new distributed algorithm for selection problem aiming to reduce the communication cost. In a model similar to the $k$-machine model (but without explicit bandwidth constraints) they present an algorithm that runs in $O(\log(k\ell))$ rounds\onlyLong{ and $O(k\log(k\ell)\log \ell)$ message complexity. Their algorithm is deterministic and uses a technique of weighted median}. \longShort{

There are several other works that  investigate applications of $\ell$-nearest neighbors, e.g., see \cite{gao2018demystifying, yu2005monitoring}. Liu et al. in \cite{liu2007clustering} applied $\ell$- nearest neighbors for processing large scale image processing.  Yang et al. \cite{yang2018efficient}  find $\ell$-nearest neighbor objects over moving objects on a large-scale data set. }{There are several other works that  investigate applications of $\ell$-nearest neighbors, e.g., see \cite{gao2018demystifying, yu2005monitoring, yang2018efficient, gao2018demystifying}.}
}

\onlyLong{We remark that in the sequential setting, {\em k-d tree} (short for $k$-dimensional tree) is a well-studied space-partitioning data structure that is used to speed up the processing of nearest neighbor queries \cite{bentley,friedman}.
While k-d tree can help in speeding up computation in the sequential setting, in the $k$-machine model we are concerned only on minimizing the number of communication rounds (and ignoring local computation within a machine). \onlyLong{In the sequential setting, under certain assumptions  k-d tree can give even logarithmic complexity per query point\cite{friedman}.
Here, as far as the round complexity is concerned, this does not matter, since we  can simply send the query point to all machines (takes 1 round)  who then locally compute the distances from the query point to their respective points  and then find the nearest neighbors in 
$O(\log \ell)$ rounds (does not depend on $k$, the number of machines) using our algorithm. As mentioned earlier, this round complexity is tight in general. }Patwary et al. in \cite{patwary2016panda} used the k-d tree to achieve faster $\ell$-NN calculation in distributed setting.\onlyLong{ They 
implemented a distributed $\ell$-NN based on k-d tree that parallelizes both k-d tree
construction and querying. They created a
large k-d tree for all the points that necessarily involves
global redistribution of points in their k-d tree construction phase.} Since their dimension based redistribution depends on the distribution of input data, their message \onlyLong{and runtime complexity (communication over network) }would be costly.\onlyLong{ Their algorithm would even experience a high round complexity in their construction phase until each node has a non-overlapping subset of input data.}
}
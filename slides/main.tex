\documentclass{beamer}
\usepackage{tikz}
\usetikzlibrary{decorations.pathmorphing}
\usetikzlibrary{positioning}
\usepackage{caption}
\usepackage{algorithm}
\usepackage[noend]{algpseudocode}
\usepackage{amsmath, amsthm, amssymb}
\usepackage{mathtools} 
\usepackage{relsize}
\usepackage{graphicx}

\usetheme{metropolis}
\metroset{
  titleformat=regular,
  progressbar=frametitle
}

\begin{document}

\title{
  \centering
  Brief Announcement: Efficient Distributed Algorithms for the $K$-Nearest Neighbors Problem
}

\author{
  \centering
  \large{Reza Fathi},  University of Houston \and \\
  \large{Anisur Rahaman Molla}, Indian Statistical Institute \and \\
  \large{Gopal Pandurangan}, University of Houston 
}

%\institute[shortinst]{\centering \inst{1} University of Houston \and %
 %                     \inst{2} Indian Statistical Institute}

% \date{\centering Jul-2020}
\date{}
{
\setbeamertemplate{footline}{}
\begin{frame}[noframenumbering]
  \maketitle
  \begin{tikzpicture}[overlay, remember picture]
    \node[above right=3mm and 6mm of current page.south west]
    {\scriptsize SPAA 2020};
  \end{tikzpicture}
\end{frame}
}

\begin{frame}{$K$-Nearest Neighbors ($K$-NN) Problem}
\begin{itemize}
 \item Given a (training) set of $n$ data points   and a query point $q$, the goal is to find $K$ points in the set that are nearest (in some distance metric)  to the query point $q$.
 
    \item A well-studied problem
in machine learning with numerous applications.
    \item Our goal is to design efficient distributed algorithms motivated by Big Data and privacy applications.
  \end{itemize}
\end{frame}

\begin{frame}{Distributed Model}
\begin{itemize}
  \item We study the problem in the {\em $k$-machine model}. 
  \item To avoid confusion, between $K$ and $k$, which are unrelated, we will say $\ell$-nearest neighbors.
  \item All machines are pairwise interconnected.
  \item $n$ points are distributed across the $k$-machines (in some arbitrary fashion).
  % \item Distributed algorithm: Synchronous Congested model. 
  \item Computation proceeds  in rounds. In a round, each machine:
    \begin{enumerate}
      \item Does its local computation.
      \item Exchanges messages with all other machines (messages are of small size, say $O(\log n)$ bits, over each edge per round).
      \item We only send distance values and not points (which may be high dimensional).
    \end{enumerate}
  \item Goal: To reduce the number of communication rounds.
\end{itemize}
\end{frame}

\begin{frame}{Our Results}
\begin{itemize}
  \item A randomized algorithm in the $k$-machine model that runs in $O(\log \ell)$ communication rounds with high probability (regardless of the number of machines $k$).
  \item Our bounds are essentially the best possible for comparison-based algorithms.
  \item There is a lower bound of $\Omega(\log n)$ communication rounds (if only one element is allowed to be exchanged per round) for finding the {\em median} of $2n$ elements distributed evenly among two processors.
\end{itemize}
\end{frame}

\begin{frame}{The Selection Problem}
$\ell$-NN problem boils down to the selection problem:
\begin{itemize}
  \item Consider the set $S$ of values $d_i$, where $d_i$ ($1 \leq i \leq n$) is the distance
  of the $i$th point to the query point. 
  \item Find the $\ell$th smallest value in the set $S$, say, $d_{\ell}$.
  \item Each machine outputs points that have distance $\leq d_{\ell}$.
\end{itemize}
\end{frame}

\begin{frame}{The Selection Algorithm}
  \begin{itemize}
    \item Each machine (which knows query point $q$) need only consider the $\ell$ nearest points to $q$. Hence total number of candidate points is $k\ell$.
    \item Implement a  randomized distributed selection algorithm that finds
    the $\ell$th smallest value in $O(\log (k\ell))$ rounds.
    \begin{itemize}
        \item Sample a random element as pivot and partition  values into two sets based on the pivot.
        \item Recurse on the appropriate set.
        \item The size of the set decreases by a constant factor on average.
    \end{itemize}
    \item  We use  sampling in order to get a faster running time, i.e.,
    $O(\log \ell)$ rounds.
  \end{itemize}
\end{frame}

\begin{frame}{Our Algorithm}
We use sampling to reduce the number of candidate points from $O(k \ell)$ to $O(\ell)$.
  \begin{itemize}
    \item Each machine randomly samples $12\log \ell$ points from its own set of points and sends them to one (leader) machine.
    \item  Leader sorts these $12k\log (\ell)$ based on their distance from $q$. Let $r$ be the point at index $21\log(\ell)$ in the sorted order.
		\item Leader broadcasts point $r$.
		\item Each machine  removes any point larger than $r$.
		\item Run the selection algorithm on the remaining set
		of points.
  \end{itemize}
\end{frame}

\iffalse
\begin{frame}{Sorted Sampled Points}
  \begin{figure}
    \centering
    \includegraphics[width=\linewidth]{fig2.pdf}
    \caption{Data sorted based on their distance from the query point $q$.}
  \end{figure}
  \begin{itemize}
    \item A good pivot belongs to any $B_i$, $2 \leq i \leq 11$.
  \end{itemize}
\end{frame}
\fi

% \begin{frame}{Run-Time Theorem}
%   \begin{theorem}\label{thm:main-fast}
%     Our Algorithm computes $\ell$-NN in $O(\log (\ell))$ rounds and using  $O(k\log (\ell))$ messages with high probability (in $\ell$).
% \end{theorem}
% \end{frame}

\begin{frame}{Experimental Results}
  \begin{figure}
    \centering
    \includegraphics[width=0.73\linewidth]{per.png}
    \caption{Run-time performance of our algorithm compared to the simple method. X-axis shows the number of $\ell$-nearest neighbors w.r.t. a query point and Y-axis shows the execution time ratio of the simple method over our algorithm.}
  \end{figure}
\end{frame}

\begin{frame}{Conclusion}
  \begin{itemize}
    \item We introduce an efficient distributed algorithm which runs in $O(\log K)$ rounds.
    \item The algorithm also uses a small number of messages, incurring only $O(k\log(K))$ messages.
    \item Our bounds are essentially the best possible for comparison-based algorithms.
    \item Our algorithm can be used as a subroutine for many other problems.
  \end{itemize}
\end{frame}

\begin{frame} {Questions}
  \Huge{\centerline{Thanks for your attention!}}
  \begin{figure}
    \includegraphics[width=0.3\textwidth]{q2.png}
  \end{figure}
\end{frame}

\end{document}
